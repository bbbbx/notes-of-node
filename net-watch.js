'use strict'
const fs = require('fs');
const filename = process.argv[2];
const net = require('net');

if (!filename) {
	throw Error('Error: No filename specified.');
}

const server = net.createServer(connection => {
	console.log('Subscriber connected.');
	connection.write(`Now watching ${filename} for changes ... \n`);

	const watch = fs.watch(filename, () => connection.write(`File changed ${new Date()}\n`));
	connection.on('close', () => {
		console.log('Subscriber disconnected.');
		watch.close();
	});
});

server.listen(60300, () => {
	console.log('Listening for subscribers...');
});
