'use strict'
const fs = require('fs');
const spawn = require('child_process').spawn;
const filename = process.argv[2];

fs.writeFile(filename, 'hello world', (err) => {
	if (err) {
		throw err;
	}
	console.log('File saved');
});
